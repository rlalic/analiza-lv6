﻿namespace WindowsFormsApp2
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button0 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.buttonExit = new System.Windows.Forms.Button();
			this.buttonClearEntity = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonEquals = new System.Windows.Forms.Button();
			this.buttonPlus = new System.Windows.Forms.Button();
			this.buttonPuta = new System.Windows.Forms.Button();
			this.buttonMinus = new System.Windows.Forms.Button();
			this.buttonDjeli = new System.Windows.Forms.Button();
			this.buttonLog = new System.Windows.Forms.Button();
			this.buttonSqrt = new System.Windows.Forms.Button();
			this.buttonFact = new System.Windows.Forms.Button();
			this.buttonPoint = new System.Windows.Forms.Button();
			this.buttonCos = new System.Windows.Forms.Button();
			this.buttonSin = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(29, 57);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(254, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "0";
			this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(311, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(0, 13);
			this.label1.TabIndex = 1;
			// 
			// button0
			// 
			this.button0.Location = new System.Drawing.Point(10, 283);
			this.button0.Name = "button0";
			this.button0.Size = new System.Drawing.Size(75, 23);
			this.button0.TabIndex = 2;
			this.button0.Text = "0";
			this.button0.UseVisualStyleBackColor = true;
			this.button0.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(10, 254);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(30, 23);
			this.button1.TabIndex = 3;
			this.button1.Text = "1";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(46, 254);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(30, 23);
			this.button2.TabIndex = 4;
			this.button2.Text = "2";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(82, 254);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(30, 23);
			this.button3.TabIndex = 5;
			this.button3.Text = "3";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(82, 196);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(30, 23);
			this.button9.TabIndex = 8;
			this.button9.Text = "9";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(46, 196);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(30, 23);
			this.button8.TabIndex = 7;
			this.button8.Text = "8";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(10, 196);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(30, 23);
			this.button7.TabIndex = 6;
			this.button7.Text = "7";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(82, 225);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(30, 23);
			this.button6.TabIndex = 11;
			this.button6.Text = "6";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(46, 225);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(30, 23);
			this.button5.TabIndex = 10;
			this.button5.Text = "5";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.ButtonClick);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(10, 225);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(30, 23);
			this.button4.TabIndex = 9;
			this.button4.Text = "4";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.ButtonClick);
			// 
			// buttonExit
			// 
			this.buttonExit.Location = new System.Drawing.Point(375, 315);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.Size = new System.Drawing.Size(75, 23);
			this.buttonExit.TabIndex = 12;
			this.buttonExit.Text = "Exit";
			this.buttonExit.UseVisualStyleBackColor = true;
			this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
			// 
			// buttonClearEntity
			// 
			this.buttonClearEntity.Location = new System.Drawing.Point(375, 242);
			this.buttonClearEntity.Name = "buttonClearEntity";
			this.buttonClearEntity.Size = new System.Drawing.Size(52, 23);
			this.buttonClearEntity.TabIndex = 13;
			this.buttonClearEntity.Text = "CE";
			this.buttonClearEntity.UseVisualStyleBackColor = true;
			this.buttonClearEntity.Click += new System.EventHandler(this.buttonClearEntity_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Location = new System.Drawing.Point(375, 213);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.Size = new System.Drawing.Size(52, 23);
			this.buttonClear.TabIndex = 14;
			this.buttonClear.Text = "C";
			this.buttonClear.UseVisualStyleBackColor = true;
			this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
			// 
			// buttonEquals
			// 
			this.buttonEquals.Location = new System.Drawing.Point(375, 148);
			this.buttonEquals.Name = "buttonEquals";
			this.buttonEquals.Size = new System.Drawing.Size(52, 59);
			this.buttonEquals.TabIndex = 15;
			this.buttonEquals.Text = "=";
			this.buttonEquals.UseVisualStyleBackColor = true;
			this.buttonEquals.Click += new System.EventHandler(this.buttonEquals_Click);
			// 
			// buttonPlus
			// 
			this.buttonPlus.Location = new System.Drawing.Point(173, 196);
			this.buttonPlus.Name = "buttonPlus";
			this.buttonPlus.Size = new System.Drawing.Size(49, 23);
			this.buttonPlus.TabIndex = 16;
			this.buttonPlus.Text = "+";
			this.buttonPlus.UseVisualStyleBackColor = true;
			this.buttonPlus.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonPuta
			// 
			this.buttonPuta.Location = new System.Drawing.Point(173, 242);
			this.buttonPuta.Name = "buttonPuta";
			this.buttonPuta.Size = new System.Drawing.Size(49, 23);
			this.buttonPuta.TabIndex = 17;
			this.buttonPuta.Text = "*";
			this.buttonPuta.UseVisualStyleBackColor = true;
			this.buttonPuta.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonMinus
			// 
			this.buttonMinus.Location = new System.Drawing.Point(251, 196);
			this.buttonMinus.Name = "buttonMinus";
			this.buttonMinus.Size = new System.Drawing.Size(49, 26);
			this.buttonMinus.TabIndex = 18;
			this.buttonMinus.Text = "-";
			this.buttonMinus.UseVisualStyleBackColor = true;
			this.buttonMinus.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonDjeli
			// 
			this.buttonDjeli.Location = new System.Drawing.Point(251, 242);
			this.buttonDjeli.Name = "buttonDjeli";
			this.buttonDjeli.Size = new System.Drawing.Size(49, 23);
			this.buttonDjeli.TabIndex = 19;
			this.buttonDjeli.Text = "/";
			this.buttonDjeli.UseVisualStyleBackColor = true;
			this.buttonDjeli.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonLog
			// 
			this.buttonLog.Location = new System.Drawing.Point(10, 104);
			this.buttonLog.Name = "buttonLog";
			this.buttonLog.Size = new System.Drawing.Size(49, 23);
			this.buttonLog.TabIndex = 20;
			this.buttonLog.Text = "log";
			this.buttonLog.UseVisualStyleBackColor = true;
			this.buttonLog.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonSqrt
			// 
			this.buttonSqrt.Location = new System.Drawing.Point(82, 104);
			this.buttonSqrt.Name = "buttonSqrt";
			this.buttonSqrt.Size = new System.Drawing.Size(49, 23);
			this.buttonSqrt.TabIndex = 21;
			this.buttonSqrt.Text = "sqrt";
			this.buttonSqrt.UseVisualStyleBackColor = true;
			this.buttonSqrt.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonFact
			// 
			this.buttonFact.Location = new System.Drawing.Point(156, 104);
			this.buttonFact.Name = "buttonFact";
			this.buttonFact.Size = new System.Drawing.Size(49, 23);
			this.buttonFact.TabIndex = 22;
			this.buttonFact.Text = "tan";
			this.buttonFact.UseVisualStyleBackColor = true;
			this.buttonFact.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonPoint
			// 
			this.buttonPoint.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.buttonPoint.Location = new System.Drawing.Point(91, 283);
			this.buttonPoint.Name = "buttonPoint";
			this.buttonPoint.Size = new System.Drawing.Size(40, 37);
			this.buttonPoint.TabIndex = 23;
			this.buttonPoint.Text = ".";
			this.buttonPoint.UseVisualStyleBackColor = true;
			this.buttonPoint.Click += new System.EventHandler(this.ButtonClick);
			// 
			// buttonCos
			// 
			this.buttonCos.Location = new System.Drawing.Point(82, 148);
			this.buttonCos.Name = "buttonCos";
			this.buttonCos.Size = new System.Drawing.Size(49, 23);
			this.buttonCos.TabIndex = 25;
			this.buttonCos.Text = "cos";
			this.buttonCos.UseVisualStyleBackColor = true;
			this.buttonCos.Click += new System.EventHandler(this.OperatorClick);
			// 
			// buttonSin
			// 
			this.buttonSin.Location = new System.Drawing.Point(8, 148);
			this.buttonSin.Name = "buttonSin";
			this.buttonSin.Size = new System.Drawing.Size(49, 23);
			this.buttonSin.TabIndex = 24;
			this.buttonSin.Text = "sin";
			this.buttonSin.UseVisualStyleBackColor = true;
			this.buttonSin.Click += new System.EventHandler(this.OperatorClick);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(462, 350);
			this.Controls.Add(this.buttonCos);
			this.Controls.Add(this.buttonSin);
			this.Controls.Add(this.buttonPoint);
			this.Controls.Add(this.buttonFact);
			this.Controls.Add(this.buttonSqrt);
			this.Controls.Add(this.buttonLog);
			this.Controls.Add(this.buttonDjeli);
			this.Controls.Add(this.buttonMinus);
			this.Controls.Add(this.buttonPuta);
			this.Controls.Add(this.buttonPlus);
			this.Controls.Add(this.buttonEquals);
			this.Controls.Add(this.buttonClear);
			this.Controls.Add(this.buttonClearEntity);
			this.Controls.Add(this.buttonExit);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.button0);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.textBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button0;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button buttonExit;
		private System.Windows.Forms.Button buttonClearEntity;
		private System.Windows.Forms.Button buttonClear;
		private System.Windows.Forms.Button buttonEquals;
		private System.Windows.Forms.Button buttonPlus;
		private System.Windows.Forms.Button buttonPuta;
		private System.Windows.Forms.Button buttonMinus;
		private System.Windows.Forms.Button buttonDjeli;
		private System.Windows.Forms.Button buttonLog;
		private System.Windows.Forms.Button buttonSqrt;
		private System.Windows.Forms.Button buttonFact;
		private System.Windows.Forms.Button buttonPoint;
		private System.Windows.Forms.Button buttonCos;
		private System.Windows.Forms.Button buttonSin;
	}
}

