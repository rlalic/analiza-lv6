﻿/*
Napravite  aplikaciju  znanstveni  kalkulator  koja  ce  imati  funkcionalnost 
znanstvenog  kalkulatora,  odnosno  implementirati  osnovne  (+,-,*,/)  i  barem  5 
naprednih (sin, cos, log, sqrt...) operacija. 
*/


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
	public partial class Form1 : Form
	{
		Double rezultat = 0;
		String operacija = "";
		bool izvedena= false;

		public Form1()
		{
			InitializeComponent();
		}

		private void ButtonClick(object sender, EventArgs e)
		{
			if ((textBox1.Text == "0") || (izvedena))
				textBox1.Clear();
			izvedena = false;
			Button button = (Button)sender;
			if (button.Text == ".")
			{
				if(!textBox1.Text.Contains("."))
					textBox1.Text = textBox1.Text + button.Text;
			} else
				textBox1.Text = textBox1.Text + button.Text;

		}

		private void OperatorClick(object sender, EventArgs e)
		{
			Button button = (Button)sender;
			if (rezultat != 0)
			{
				buttonEquals.PerformClick();
				operacija = button.Text;
				label1.Text = rezultat + " " + operacija;
				izvedena = true;
			}
			else
			{
				operacija = button.Text;
				rezultat = Double.Parse(textBox1.Text);
				label1.Text = rezultat + " " + operacija;
				izvedena = true;
			}
		}

		private void buttonClearEntity_Click(object sender, EventArgs e)
		{
			textBox1.Text = "0";
		}

		private void buttonClear_Click(object sender, EventArgs e)
		{
			textBox1.Text = "0";
			rezultat = 0;
		}

		private void buttonEquals_Click(object sender, EventArgs e)
		{
			switch (operacija)
			{
				case "+":
					textBox1.Text = (rezultat + Double.Parse(textBox1.Text)).ToString();
					break;
				case "-":
					textBox1.Text = (rezultat - Double.Parse(textBox1.Text)).ToString();
					break;
				case "*":
					textBox1.Text = (rezultat * Double.Parse(textBox1.Text)).ToString();
					break;
				case "/":
					textBox1.Text = (rezultat / Double.Parse(textBox1.Text)).ToString();
					break;
				case "log":
					textBox1.Text = (Math.Log10(rezultat)).ToString();
					break;
				case "sqrt":
					textBox1.Text = (Math.Sqrt(rezultat)).ToString();
					break;
				case "tan":
					textBox1.Text = (Math.Tan(rezultat)).ToString();
					break;
				case "sin":
					textBox1.Text = (Math.Sin(rezultat)).ToString();
					break;
				case "cos":
					textBox1.Text = (Math.Cos(rezultat)).ToString();
					break;
				default:
					break;
			}
			rezultat = Double.Parse(textBox1.Text);
			label1.Text = "";
		}

		private void buttonExit_Click(object sender, EventArgs e)
		{
			Application.Exit();
		}
	}
}
