﻿namespace WindowsFormsApp5
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelWord = new System.Windows.Forms.Label();
			this.buttonA = new System.Windows.Forms.Button();
			this.buttonB = new System.Windows.Forms.Button();
			this.buttonC = new System.Windows.Forms.Button();
			this.buttonD = new System.Windows.Forms.Button();
			this.buttonE = new System.Windows.Forms.Button();
			this.buttonF = new System.Windows.Forms.Button();
			this.buttonG = new System.Windows.Forms.Button();
			this.buttonH = new System.Windows.Forms.Button();
			this.buttonI = new System.Windows.Forms.Button();
			this.buttonJ = new System.Windows.Forms.Button();
			this.buttonK = new System.Windows.Forms.Button();
			this.buttonV = new System.Windows.Forms.Button();
			this.buttonU = new System.Windows.Forms.Button();
			this.buttonT = new System.Windows.Forms.Button();
			this.buttonS = new System.Windows.Forms.Button();
			this.buttonR = new System.Windows.Forms.Button();
			this.buttonQ = new System.Windows.Forms.Button();
			this.buttonP = new System.Windows.Forms.Button();
			this.buttonO = new System.Windows.Forms.Button();
			this.buttonN = new System.Windows.Forms.Button();
			this.buttonM = new System.Windows.Forms.Button();
			this.buttonL = new System.Windows.Forms.Button();
			this.buttonZ = new System.Windows.Forms.Button();
			this.buttonY = new System.Windows.Forms.Button();
			this.buttonX = new System.Windows.Forms.Button();
			this.buttonW = new System.Windows.Forms.Button();
			this.buttonNewGame = new System.Windows.Forms.Button();
			this.labelLives = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelWord
			// 
			this.labelWord.AutoSize = true;
			this.labelWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelWord.Location = new System.Drawing.Point(126, 61);
			this.labelWord.Name = "labelWord";
			this.labelWord.Size = new System.Drawing.Size(81, 29);
			this.labelWord.TabIndex = 0;
			this.labelWord.Text = "label1";
			// 
			// buttonA
			// 
			this.buttonA.Location = new System.Drawing.Point(37, 276);
			this.buttonA.Name = "buttonA";
			this.buttonA.Size = new System.Drawing.Size(33, 23);
			this.buttonA.TabIndex = 1;
			this.buttonA.Text = "A";
			this.buttonA.UseVisualStyleBackColor = true;
			this.buttonA.Click += new System.EventHandler(this.buttonA_Click);
			// 
			// buttonB
			// 
			this.buttonB.Location = new System.Drawing.Point(76, 276);
			this.buttonB.Name = "buttonB";
			this.buttonB.Size = new System.Drawing.Size(33, 23);
			this.buttonB.TabIndex = 2;
			this.buttonB.Text = "B";
			this.buttonB.UseVisualStyleBackColor = true;
			this.buttonB.Click += new System.EventHandler(this.buttonB_Click);
			// 
			// buttonC
			// 
			this.buttonC.Location = new System.Drawing.Point(115, 276);
			this.buttonC.Name = "buttonC";
			this.buttonC.Size = new System.Drawing.Size(33, 23);
			this.buttonC.TabIndex = 3;
			this.buttonC.Text = "C";
			this.buttonC.UseVisualStyleBackColor = true;
			this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
			// 
			// buttonD
			// 
			this.buttonD.Location = new System.Drawing.Point(154, 276);
			this.buttonD.Name = "buttonD";
			this.buttonD.Size = new System.Drawing.Size(33, 23);
			this.buttonD.TabIndex = 4;
			this.buttonD.Text = "D";
			this.buttonD.UseVisualStyleBackColor = true;
			this.buttonD.Click += new System.EventHandler(this.buttonD_Click);
			// 
			// buttonE
			// 
			this.buttonE.Location = new System.Drawing.Point(193, 276);
			this.buttonE.Name = "buttonE";
			this.buttonE.Size = new System.Drawing.Size(33, 23);
			this.buttonE.TabIndex = 5;
			this.buttonE.Text = "E";
			this.buttonE.UseVisualStyleBackColor = true;
			this.buttonE.Click += new System.EventHandler(this.buttonE_Click);
			// 
			// buttonF
			// 
			this.buttonF.Location = new System.Drawing.Point(232, 276);
			this.buttonF.Name = "buttonF";
			this.buttonF.Size = new System.Drawing.Size(33, 23);
			this.buttonF.TabIndex = 6;
			this.buttonF.Text = "F";
			this.buttonF.UseVisualStyleBackColor = true;
			this.buttonF.Click += new System.EventHandler(this.buttonF_Click);
			// 
			// buttonG
			// 
			this.buttonG.Location = new System.Drawing.Point(271, 276);
			this.buttonG.Name = "buttonG";
			this.buttonG.Size = new System.Drawing.Size(33, 23);
			this.buttonG.TabIndex = 7;
			this.buttonG.Text = "G";
			this.buttonG.UseVisualStyleBackColor = true;
			this.buttonG.Click += new System.EventHandler(this.buttonG_Click);
			// 
			// buttonH
			// 
			this.buttonH.Location = new System.Drawing.Point(310, 276);
			this.buttonH.Name = "buttonH";
			this.buttonH.Size = new System.Drawing.Size(33, 23);
			this.buttonH.TabIndex = 8;
			this.buttonH.Text = "H";
			this.buttonH.UseVisualStyleBackColor = true;
			this.buttonH.Click += new System.EventHandler(this.buttonH_Click);
			// 
			// buttonI
			// 
			this.buttonI.Location = new System.Drawing.Point(349, 276);
			this.buttonI.Name = "buttonI";
			this.buttonI.Size = new System.Drawing.Size(33, 23);
			this.buttonI.TabIndex = 9;
			this.buttonI.Text = "I";
			this.buttonI.UseVisualStyleBackColor = true;
			this.buttonI.Click += new System.EventHandler(this.buttonI_Click);
			// 
			// buttonJ
			// 
			this.buttonJ.Location = new System.Drawing.Point(388, 276);
			this.buttonJ.Name = "buttonJ";
			this.buttonJ.Size = new System.Drawing.Size(33, 23);
			this.buttonJ.TabIndex = 10;
			this.buttonJ.Text = "J";
			this.buttonJ.UseVisualStyleBackColor = true;
			this.buttonJ.Click += new System.EventHandler(this.buttonJ_Click);
			// 
			// buttonK
			// 
			this.buttonK.Location = new System.Drawing.Point(427, 276);
			this.buttonK.Name = "buttonK";
			this.buttonK.Size = new System.Drawing.Size(33, 23);
			this.buttonK.TabIndex = 11;
			this.buttonK.Text = "K";
			this.buttonK.UseVisualStyleBackColor = true;
			this.buttonK.Click += new System.EventHandler(this.buttonK_Click);
			// 
			// buttonV
			// 
			this.buttonV.Location = new System.Drawing.Point(427, 318);
			this.buttonV.Name = "buttonV";
			this.buttonV.Size = new System.Drawing.Size(33, 23);
			this.buttonV.TabIndex = 22;
			this.buttonV.Text = "V";
			this.buttonV.UseVisualStyleBackColor = true;
			this.buttonV.Click += new System.EventHandler(this.buttonV_Click);
			// 
			// buttonU
			// 
			this.buttonU.Location = new System.Drawing.Point(388, 318);
			this.buttonU.Name = "buttonU";
			this.buttonU.Size = new System.Drawing.Size(33, 23);
			this.buttonU.TabIndex = 21;
			this.buttonU.Text = "U";
			this.buttonU.UseVisualStyleBackColor = true;
			this.buttonU.Click += new System.EventHandler(this.buttonU_Click);
			// 
			// buttonT
			// 
			this.buttonT.Location = new System.Drawing.Point(349, 318);
			this.buttonT.Name = "buttonT";
			this.buttonT.Size = new System.Drawing.Size(33, 23);
			this.buttonT.TabIndex = 20;
			this.buttonT.Text = "T";
			this.buttonT.UseVisualStyleBackColor = true;
			this.buttonT.Click += new System.EventHandler(this.buttonT_Click);
			// 
			// buttonS
			// 
			this.buttonS.Location = new System.Drawing.Point(310, 318);
			this.buttonS.Name = "buttonS";
			this.buttonS.Size = new System.Drawing.Size(33, 23);
			this.buttonS.TabIndex = 19;
			this.buttonS.Text = "S";
			this.buttonS.UseVisualStyleBackColor = true;
			this.buttonS.Click += new System.EventHandler(this.buttonS_Click);
			// 
			// buttonR
			// 
			this.buttonR.Location = new System.Drawing.Point(271, 318);
			this.buttonR.Name = "buttonR";
			this.buttonR.Size = new System.Drawing.Size(33, 23);
			this.buttonR.TabIndex = 18;
			this.buttonR.Text = "R";
			this.buttonR.UseVisualStyleBackColor = true;
			this.buttonR.Click += new System.EventHandler(this.buttonR_Click);
			// 
			// buttonQ
			// 
			this.buttonQ.Location = new System.Drawing.Point(232, 318);
			this.buttonQ.Name = "buttonQ";
			this.buttonQ.Size = new System.Drawing.Size(33, 23);
			this.buttonQ.TabIndex = 17;
			this.buttonQ.Text = "Q";
			this.buttonQ.UseVisualStyleBackColor = true;
			this.buttonQ.Click += new System.EventHandler(this.buttonQ_Click);
			// 
			// buttonP
			// 
			this.buttonP.Location = new System.Drawing.Point(193, 318);
			this.buttonP.Name = "buttonP";
			this.buttonP.Size = new System.Drawing.Size(33, 23);
			this.buttonP.TabIndex = 16;
			this.buttonP.Text = "P";
			this.buttonP.UseVisualStyleBackColor = true;
			this.buttonP.Click += new System.EventHandler(this.buttonP_Click);
			// 
			// buttonO
			// 
			this.buttonO.Location = new System.Drawing.Point(154, 318);
			this.buttonO.Name = "buttonO";
			this.buttonO.Size = new System.Drawing.Size(33, 23);
			this.buttonO.TabIndex = 15;
			this.buttonO.Text = "O";
			this.buttonO.UseVisualStyleBackColor = true;
			this.buttonO.Click += new System.EventHandler(this.buttonO_Click);
			// 
			// buttonN
			// 
			this.buttonN.Location = new System.Drawing.Point(115, 318);
			this.buttonN.Name = "buttonN";
			this.buttonN.Size = new System.Drawing.Size(33, 23);
			this.buttonN.TabIndex = 14;
			this.buttonN.Text = "N";
			this.buttonN.UseVisualStyleBackColor = true;
			this.buttonN.Click += new System.EventHandler(this.buttonN_Click);
			// 
			// buttonM
			// 
			this.buttonM.Location = new System.Drawing.Point(76, 318);
			this.buttonM.Name = "buttonM";
			this.buttonM.Size = new System.Drawing.Size(33, 23);
			this.buttonM.TabIndex = 13;
			this.buttonM.Text = "M";
			this.buttonM.UseVisualStyleBackColor = true;
			this.buttonM.Click += new System.EventHandler(this.buttonM_Click);
			// 
			// buttonL
			// 
			this.buttonL.Location = new System.Drawing.Point(37, 318);
			this.buttonL.Name = "buttonL";
			this.buttonL.Size = new System.Drawing.Size(33, 23);
			this.buttonL.TabIndex = 12;
			this.buttonL.Text = "L";
			this.buttonL.UseVisualStyleBackColor = true;
			this.buttonL.Click += new System.EventHandler(this.buttonL_Click);
			// 
			// buttonZ
			// 
			this.buttonZ.Location = new System.Drawing.Point(154, 360);
			this.buttonZ.Name = "buttonZ";
			this.buttonZ.Size = new System.Drawing.Size(33, 23);
			this.buttonZ.TabIndex = 26;
			this.buttonZ.Text = "Z";
			this.buttonZ.UseVisualStyleBackColor = true;
			this.buttonZ.Click += new System.EventHandler(this.buttonZ_Click);
			// 
			// buttonY
			// 
			this.buttonY.Location = new System.Drawing.Point(115, 360);
			this.buttonY.Name = "buttonY";
			this.buttonY.Size = new System.Drawing.Size(33, 23);
			this.buttonY.TabIndex = 25;
			this.buttonY.Text = "Y";
			this.buttonY.UseVisualStyleBackColor = true;
			this.buttonY.Click += new System.EventHandler(this.buttonY_Click);
			// 
			// buttonX
			// 
			this.buttonX.Location = new System.Drawing.Point(76, 360);
			this.buttonX.Name = "buttonX";
			this.buttonX.Size = new System.Drawing.Size(33, 23);
			this.buttonX.TabIndex = 24;
			this.buttonX.Text = "X";
			this.buttonX.UseVisualStyleBackColor = true;
			this.buttonX.Click += new System.EventHandler(this.buttonX_Click);
			// 
			// buttonW
			// 
			this.buttonW.Location = new System.Drawing.Point(37, 360);
			this.buttonW.Name = "buttonW";
			this.buttonW.Size = new System.Drawing.Size(33, 23);
			this.buttonW.TabIndex = 23;
			this.buttonW.Text = "W";
			this.buttonW.UseVisualStyleBackColor = true;
			this.buttonW.Click += new System.EventHandler(this.buttonW_Click);
			// 
			// buttonNewGame
			// 
			this.buttonNewGame.Location = new System.Drawing.Point(489, 120);
			this.buttonNewGame.Name = "buttonNewGame";
			this.buttonNewGame.Size = new System.Drawing.Size(111, 23);
			this.buttonNewGame.TabIndex = 27;
			this.buttonNewGame.Text = "New game";
			this.buttonNewGame.UseVisualStyleBackColor = true;
			this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
			// 
			// labelLives
			// 
			this.labelLives.AutoSize = true;
			this.labelLives.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelLives.Location = new System.Drawing.Point(508, 33);
			this.labelLives.Name = "labelLives";
			this.labelLives.Size = new System.Drawing.Size(81, 29);
			this.labelLives.TabIndex = 28;
			this.labelLives.Text = "label2";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.labelLives);
			this.Controls.Add(this.buttonNewGame);
			this.Controls.Add(this.buttonZ);
			this.Controls.Add(this.buttonY);
			this.Controls.Add(this.buttonX);
			this.Controls.Add(this.buttonW);
			this.Controls.Add(this.buttonV);
			this.Controls.Add(this.buttonU);
			this.Controls.Add(this.buttonT);
			this.Controls.Add(this.buttonS);
			this.Controls.Add(this.buttonR);
			this.Controls.Add(this.buttonQ);
			this.Controls.Add(this.buttonP);
			this.Controls.Add(this.buttonO);
			this.Controls.Add(this.buttonN);
			this.Controls.Add(this.buttonM);
			this.Controls.Add(this.buttonL);
			this.Controls.Add(this.buttonK);
			this.Controls.Add(this.buttonJ);
			this.Controls.Add(this.buttonI);
			this.Controls.Add(this.buttonH);
			this.Controls.Add(this.buttonG);
			this.Controls.Add(this.buttonF);
			this.Controls.Add(this.buttonE);
			this.Controls.Add(this.buttonD);
			this.Controls.Add(this.buttonC);
			this.Controls.Add(this.buttonB);
			this.Controls.Add(this.buttonA);
			this.Controls.Add(this.labelWord);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelWord;
		private System.Windows.Forms.Button buttonA;
		private System.Windows.Forms.Button buttonB;
		private System.Windows.Forms.Button buttonC;
		private System.Windows.Forms.Button buttonD;
		private System.Windows.Forms.Button buttonE;
		private System.Windows.Forms.Button buttonF;
		private System.Windows.Forms.Button buttonG;
		private System.Windows.Forms.Button buttonH;
		private System.Windows.Forms.Button buttonI;
		private System.Windows.Forms.Button buttonJ;
		private System.Windows.Forms.Button buttonK;
		private System.Windows.Forms.Button buttonV;
		private System.Windows.Forms.Button buttonU;
		private System.Windows.Forms.Button buttonT;
		private System.Windows.Forms.Button buttonS;
		private System.Windows.Forms.Button buttonR;
		private System.Windows.Forms.Button buttonQ;
		private System.Windows.Forms.Button buttonP;
		private System.Windows.Forms.Button buttonO;
		private System.Windows.Forms.Button buttonN;
		private System.Windows.Forms.Button buttonM;
		private System.Windows.Forms.Button buttonL;
		private System.Windows.Forms.Button buttonZ;
		private System.Windows.Forms.Button buttonY;
		private System.Windows.Forms.Button buttonX;
		private System.Windows.Forms.Button buttonW;
		private System.Windows.Forms.Button buttonNewGame;
		private System.Windows.Forms.Label labelLives;
	}
}

