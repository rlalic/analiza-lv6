﻿/*
Napravite jednostavnu igru vjesala. Pojmovi se ucitavaju u  listu iz datoteke, i u 
svakoj  partiji  se  odabire  nasumicni  pojam  iz  liste.  Omoguciti  svu 
funkcionalnost koju biste ocekivali od takve igre. Nije nuzno crtati vjesala, 
dovoljno je na labeli ispisati koliko je pokusaja za odabir slova preostalo. 
 
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp5
{
	public partial class Form1 : Form
	{
		List<string> listaRijeci = new List<string>();
		string path = "ListOfWords.txt";
		int trazen;
		int lives = 10;
		int NumOfGuesses;
		string rijec;
		string length;
		System.Text.StringBuilder lengthBuild;
		public void New_Game()
		{
			length = "";
			NumOfGuesses = 0;
			lives = 10;
			Random rand = new Random();
			trazen = rand.Next(0, listaRijeci.Count);
			rijec = listaRijeci[trazen];
			for (int i = 0; i < rijec.Length; i++)
			{
				length = length + "_ ";
			}

			lengthBuild = new StringBuilder(length);
			labelLives.Text = lives.ToString();
			labelWord.Text = length;
		}

		public void CheckL()
		{
			if (lives <= 0)
			{
				MessageBox.Show("You ran out of lives!");
				New_Game();
			}

		}
		public void CheckW()
		{
			if (NumOfGuesses == rijec.Length)
				MessageBox.Show("Congratulations, you won the game!");
		}




		public Form1()
		{
			InitializeComponent();
			using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					listaRijeci.Add(line);
				}
			}
			New_Game();

		}

		private void buttonNewGame_Click(object sender, EventArgs e)
		{
			New_Game();
		}

		private void buttonA_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++) {
				if (rijec[i] == 'A') {
					NumOfGuesses++;
					lengthBuild[i * 2] = 'A';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess) {
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonB_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'B')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'B';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonC_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'C')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'C';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonD_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'D')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'D';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonE_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'E')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'E';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonF_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'F')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'F';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonG_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'G')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'G';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonH_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'H')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'H';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonI_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'I')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'I';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonJ_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'J')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'J';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonK_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'K')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'K';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonL_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'L')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'L';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonM_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'M')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'M';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonN_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'N')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'N';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonO_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'O')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'O';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonP_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'P')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'P';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonQ_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'Q')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'Q';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonR_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'R')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'R';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonS_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'S')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'S';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonT_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'T')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'T';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonU_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'U')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'U';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonV_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'V')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'V';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonW_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'W')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'W';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonX_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'X')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'X';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonY_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'Y')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'Y';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}

		private void buttonZ_Click(object sender, EventArgs e)
		{
			bool guess = false;
			for (int i = 0; i < rijec.Length; i++)
			{
				if (rijec[i] == 'Z')
				{
					NumOfGuesses++;
					lengthBuild[i * 2] = 'Z';
					length = lengthBuild.ToString();
					guess = true;
					continue;
				}
			}
			labelWord.Text = length;
			if (!guess)
			{
				lives--;
				labelLives.Text = lives.ToString();
			}
			CheckW();
			CheckL();
		}
	}
}
